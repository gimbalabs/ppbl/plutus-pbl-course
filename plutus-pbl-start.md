---
aliases: "Plutus PBL"
creation date: 2022-06-12 06:02 
modification date: Sunday 19th June 2022 08:35:36
parent: "none"
collaborators: [""]
tags: [""]
priority: 5
current: True
---
# Plutus PBL 2.0
Start + get done:
Discord --> Canvas review
	What are the opportunities to learn by doing?
		Credential
		Gimbals
		Course content
		How does this connect back to emurgo?


Remember to think about Donations to PPBL Course team
	"Pay what you want"
	"Support our team"

Remember that gimbalabs.com/team needs some updates!

## KEY DOCS
1. [[ppbl-curriculum-outline]]
2. [[role-of-mentorship]]

- [ ] Create Canvas Course Home Page


---
### Final Sprint - Logistics - Questions
1. [x] Change Gitlab repo links in 101
	1. [ ] Remake 101 videos (Sunday / Monday)
2. How will we set standards for video content?
	1. Format
	2. Streaming Location
	3. Raw files + editability (open access vs. open source)
	4. Timestamps
3. How do we handle video translation?
	1. Japanese
	2. LATAM
4. Credential Tokens
	1. Onchain Achievement Token: https://docs.galaxy.eco/into-the-galaxy/galaxy-oat/
 

6. Gimbalabs Open License from Stephen

## Week of July 18
- [ ] Update GBTE Contract + Front End
- [ ] Link to course on Gimbalabs.com + pay what you want link
- [ ] Be able to give bounties to people who contribute! (Week of July 18)
- [ ] Discord clean up
	1. Don't lose anything --> Archivist Role
	2. Simpler channels + less repetition
	3. Need PPBL Learners vs. Course Creators



## From Stephen 
"PBL Open-Source policy
Gimbalabs is committed to open-source software and content. A great deal of intellectual capital has gone into the Plutus Project Based Learning courses. Therefore, in the short term we will provide open access to our content under a closed-source licence. In the medium term we will transition to an open-source or creative commons licence."

Add "knowledge sharing" and "module feedback"



# Reflections:
Nothing can replace DYOR. In this course, we're going to show you as much as we can, but there is no end-point to our goal. We want you to be learning with us, just like we are. There is so much documentation still to write! There is so much to build. While the possibilities are endless, we are limited by our capacity to explore them. So let's go. For example: nothing I can say to you can replace the ideas you'll have when you read this blog post: https://iohk.io/en/blog/posts/2022/07/07/research-overview-part-3-tokens-stablecoins-and-fees/.

You'll want to talk about it. We know about a phenomenon where talking and doing inspire each other (is there any research here?)

# For Your Consideration:

[[Funding the Commons]] (Day 2/2)

The Public & Its Problems: On Finding Local Solutions to Global Problems
- Middle out system --> Local Centers
- Global fabric/web
- A light global protocol: just enough for coherence (see coordination)
- "resilience of biodiversity" vs. "institutional monoculture"
- Layers / circles -> provisioned permissioning
	- IE: smart contract business logic vs. smart contract code
	- This is our entry point with #PPBL


Opt In to larger system
Opt out to fork
Adapting / localizing values

## PPBL -> TPBL
### We're going to talk about Value in (at least) two ways
- [ ] To elucidate Values is the first step toward provisioning Infrastructure, which is never neutral.
- Values in shared markdown file. Teach git flow; forking; and Values process, in both human sense and Asset sense.

### Use real pedagogy:
- Open Spaces
- Fishbowls
- Unconference days
- Build this all with Emurgo Ed, with CardanOx, with Eva, and beyond.
- Really elucidate this idea that when non-coders help develop "DAPPS", 


## PEOPLE
We need people who can focus on improving some of the basics:
- Our front end template -- [[MixAxiM]]?
- Our basic contracts [[Adrian]]?
- Our understanding of serialization libraries [[Abdelkrim]]?
- Incorporating Vasil and the changes -- me? [[Genty]], everyone?
- Lace, Lucid, etc etc etc [[Piotr]], [[Abdelkrim]], who else?
- New projects: [[Darlington]], [[Littlefish]], [[Edify]]
- Don't forget [[EV]] and [[Sebastian]]

## COMPONENTS:
Let's weave fabric and get commitments from people who want to own this thing.

1. Gimbalabs DAOrg Chart [[Decentralized Autonomous Org Chart]]
2. PBL Framework [[PBL]]
3. Our own capacity to teach
4. The Projects we're working on
5. [[role-of-mentorship]]

## COLLABORATIONS --> CONTRIBUTION NODES?

```dataview
TABLE FROM "202-collaborations" and #PPBL
SORT file.name ASC
```



---
### Narrative - Why it's complicated
See [[00-intro-toc|Gimbalabs White Paper]]
- A course developing in real time with an ecosystem
- An organization developing in real time as we create new ideas and collaboration
- What we need to show is the energy of this ongoing development; the synergy of building as we go.
- AND, ENOUGH OF ALL THAT. We need brevity and clarity.
[[Nori]] - Agile development = "What is the thinnest possibe end to end slice?" (the vertical, the simplest NFT) #PPBL 
	- Here is the value, you can see it.
	- You know, I should write a little something for [[Gimbal Bounty Treasury and Escrow]]


---
# Meeting Archive
```dataview
TABLE meeting-date FROM "201-meetings/meetings" and #PPBL 
SORT meeting-date DESC
```

# Boards
(can I size images in .md)

![[pblwg-board.jpg | 800]]
